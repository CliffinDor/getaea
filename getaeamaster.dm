#modname "Getaea v 3.2"
#description "This mod introduces the nation of Getaea. In middle age, Getty is primarily a continuation of Sauro with a dash or two of Pan. This changes in late age, where Panic influence heightens and a lot of Romanian mythology can be seen. We based it off of Dacia and Romania, although we definitely took a few artistic liberties."
#version 3.2
#domversion 5.42
#icon "getaea/banner.tga"

--First, a new weapon for Dakoi Warriors.
#newweapon 900
#name "Poison Hatchet"
#dmg 5
#nratt 1
#att 1
#def 0
#len 1
#sound 10
#rcost 4
#slash
#secondaryeffect 50 -- This is a poison effect.
#ironweapon
#end

--And another new weapon, this time for Dakoi Horsemen.
#newweapon 901
#name "Poison Javelin"
#dmg 5
#nratt 1
#att -2
#range -1
#ammo 2
#rcost 4
#sound 19
#pierce
#secondaryeffect 50 -- Again, poison.
#woodenweapon
#end

--A phantasmal bite for the MA Solomonari's dragons.
#newweapon 902
#name "Phantasmal Bite"
#dmg 15
#nratt 1
#att 0
#def -1
#len 0
#sound 38
#slash
#bonus
#magic
#mrnegates
#nostr
#end

--Next, two weapons for LA. The Sica is a one-handed Falx. These weapons are known to have been used by the Dacians and Thracians.
#newweapon 903
#name "Sica"
#dmg 7
#nratt 1
#att 0
#def 0
#len 1
#flail
#sound 8
#rcost 4
#slash
#ironweapon
#end

#newweapon 904
#name "Falx"
#dmg 10
#nratt 1
#att 0
#def 0
#flail
#len 2
#twohanded
#sound 12
#rcost 5
#slash
#ironweapon
#end

=====Units=====
---MA---

--Now, time for the recruitable units.
#newmonster 5700
#name "Getaean Archer"
#nametype 136
--#descr "Like the Sauromatian tribes of yore, archers are an important part of the Getaean war machine. They no longer use poison arrows like the Androphags once did."
#descr "Fitting with their nomadic roots, archers are an important part of the Getaean war machine. As their battle tactics have begin to modernize, the use of poison arrows has fallen into disuse."
#spr1 "getaea/archer1.tga"
#spr2 "getaea/archer2.tga"
#rcost 1
#gcost 10010
#hp 11
#mr 10
#mor 11
#str 11
#att 10
#def 10
#prec 11
#enc 3
#mapmove 16
#rpcost 10000
#ap 12
#weapon 9 -- Dagger
#weapon 264 -- Composite Bow
#armor 10 -- Leather hauberk
#end

#newmonster 5701
#name "Getaean Peltast"
#nametype 136
--#descr "Unlike the tribes of Sauromatia, the nation of Getaea makes heavy use of infantry in battle. Getaean peltasts are equipped with spears and shields, and wear leather armor rather than heavy scale mail."
#descr "Arcoscephale's tactics are not forgotten.  Getaean peltasts are one example of this, despite being slightly more heavily-armored than their mother country's peltasts, their role is much the same: Hurl javelins to scatter an enemy's ranks, then support their allies in the melee."
#spr1 "getaea/lightinfantry1.tga"
#spr2 "getaea/lightinfantry2.tga"
#rcost 1
#rpcost 10000
#gcost 10012
#weapon 1
#weapon21 --javelin
#armor 11 -- Ring mail hauberk
#armor 20 -- Iron cap
#armor 2 -- Shield
--The humans of Getaea come from mixed Sauro and Arco stock, and Sauromatians were tougher than normal men.
#hp 11
#mr 10
#mor 11
#str 11
#def 11
#prec 8
#enc 3
#mapmove 16
#end

#newmonster 5702
#copystats 5701
#name "Getaean Infantry"
#nametype 136
#descr "Getaea originally utilized Arcoscephale's infantry tactics. When their mother empire eventually began to wane and Getaea was invaded by Sauromatian tribes, the use of heavily armored phalanxes in its army fell out of favor. Their infantry is no longer trained to fight in formations, but the spear and shield are still widely used."
#cleararmor
#clearweapons
#spr1 "getaea/infantry1.tga"
#spr2 "getaea/infantry2.tga"
#rcost 1
#rpcost 10000
#gcost 10012
#weapon 903 --sica
#weapon21 --javelin
#armor 12 -- Scale mail hauberk
#armor 20
#armor 2
#mapmove 16
#end
--swapped their spear for a sica as a way of increasing damage type diversity, will need a new sprite


#newmonster 5703
#copystats 5701
#name "Dakoi Warrior"
#nametype 136
#descr "The Dakos were native to Getaea even before it was settled by Arcoscephalians. Dakoi are a barbaric people who revere wolves, and their warriors wear fur and go berserk in battle. It is rumored that the Dakos learned the practice of poisoned weapons from the Androphags, and their warriors wield poisonous hatchets in combat.  They seek to emulate the swiftness of the snake and ferocity of the wolf in attack, and tattoo themselves accordingly."
#clearweapons
#cleararmor
#spr1 "getaea/dakoiwarrior1.tga"
#spr2 "getaea/dakoiwarrior2.tga"
#rcost 1
#gcost 10015
#weapon 900
#weapon 900
#armor 44 -- Fur
#armor 120 -- Leather cap
#rpcost 10000
#hp 12
#mor 13
#str 11
#att 11
#forestsurvival
#ambidextrous 1
#berserk 3
#snaketattoo 1
#wolftattoo 1
#mapmove 16
#end

#newmonster 5704
#name "Raider"
#nametype 136
#descr "When the tribes from the east invaded Getaea, they imprinted an affinity for horsemanship on the newborn nation. Raiders are warriors wealthy enough to buy their own horses. They wield spears and composite bows in combat and are very effective at pillaging enemy provinces.  They use green bronze and other materials to ritually bond themselves and their steeds."
#spr1 "getaea/raider1.tga"
#spr2 "getaea/raider2.tga"
#rcost 1
#rpcost 10000
#gcost 10010
#hp 11
#mr 10
#mor 11
#str 11
#att 10
#def 11
#prec 8
#enc 4
#mapmove 22
#ap 26
#size 3
#ressize 2
#pillagebonus 1
#mounted
#horsetattoo 1
#weapon 1
#weapon 264
#armor 11 -- Ring mail haub
#armor 118 -- Half helm
#armor 1 -- Buckler
#end

#newmonster 5705
#name "Lancer"
#nametype 136
#descr "Getaean Lancers are better trained than their raider counterparts. Their horses wear barding, and the riders themselves wear ring mail light enough to maneuver in. Lancers wield light lances and carry shields to protect themselves.  They use green bronze and other materials to ritually bond themselves and their steeds."
#spr1 "getaea/lancer1.tga"
#spr2 "getaea/lancer2.tga"
#rcost 5
#rpcost 10000
#gcost 10015
#hp 11
#mr 10
#mor 11
#str 11
#att 10
#def 11
#prec 8
#enc 4
#mapmove 22
#ap 26
#size 3
#ressize 2
#mounted
#horsetattoo 1
#weapon 357
#armor 11
#armor 118
#armor 2
#end

#newmonster 5706
#name "Getaean Cataphract"
#nametype 135
#descr "Some Getaean women yearn for greater things than bearing children and keeping house.  Those who aspire to the ranks of the Jade Cataphracts must first prove themselves in battle.  Those who prove exemplary warriors may be blessed by the Awakening God and don the sacred Jade, both on their armor and on their very skin."
#copyspr 1172
#rcost 5
#rpcost 10000
#gcost 10035
#weapon 4
#weapon 8
#weapon 56
#armor 12
#armor 2
#armor 118
#hp 11
#mr 11
#mor 13
#att 12
#def 11
#prec 8
#enc 3
#mapmove 22
#ap 24
#size 3
#ressize 2
#mounted
#horsetattoo 1
#xpshape 30
#female
#end
--Keep this armor the same and up the Jade catap/Kyria's armor slightly?
--Add an XPshape to jade cata
--Swapped the # of the dakos horseman for the cataphract so it won't break

--I want to make these a LOT cheaper because 55 gold for what you get just ain't worth it, but unfortunately the game is insanely expensive when it comes to cavalry costs, and I don't want to make this OP relative to vanilla.  Fugggg
--I also maybe want to up their armor to chain mail hauberks, item id #13
--UPDATE CAVALRY SIGNIFICANTLY BETTER THAN IN DOM4 THEY ARE WORTH THEIR NORMAL COST NOW

#newmonster 5707
#name "Jade Cataphract"
#nametype 135
#descr "While many Getaean women are simply farmwives, a few of them carry on the tradition of Sauromatia's founding tribes and become accomplished warriors. These Cataphracts go through rigorous training and dress themselves in jade colors like the Amazons of ages past. As reminders of the nation's Sauromatian heritage, Jade Cataphracts are sacred and can be blessed in combat.  They wear jade even when nude, as their magical tattoos are created with an infusion of valuable and sacred jade."
#spr1 "getaea/jadecat1.tga"
#spr2 "getaea/jadecat2.tga"
#rcost 5
#gcost 10035
#weapon 4
#weapon 8
#weapon 56
#armor 12
#armor 2
#armor 118
#hp 12
#mr 12
#mor 14
#att 12
#def 12
#prec 8
#enc 3
#mapmove 22
#rpcost 10000
#ap 24
#size 3
#ressize 2
#mounted
#holy
#female
#horsetattoo 2
#end

#newmonster 5755
#name "Dakoi Horseman"
#nametype 136
#descr "The Dakos did not widely practice horsemanship until after their lands were invaded by Sauromatians. They are lightly armored, and wield spears and javelins in combat. The javelins they throw are tipped with poison, a practice the Dakoi tribes claim to have learned from Androphag warriors of old."
#spr1 "getaea/dakoihorseman1.tga"
#spr2 "getaea/dakoihorseman2.tga"
#rcost 1
#gcost 10015
#weapon 1
#weapon 901
#armor 11
#armor 120
#hp 11
#mr 10
#mor 12
#str 11
#def 12
#prec 10
#enc 3
#mapmove 22
#ap 26
#size 3
#ressize 2
#mounted
#forestsurvival
#snaketattoo 1
#horsetattoo 1
#end


#newmonster 5708
#name "Getaean Hoplite"
#nametype 136
#descr "The Getai have assimilated several coastal provinces which were once old Arcoscephalian colonies. Kosonoi sometimes levy professional soldiers who use formation tactics in battle. Though sometimes thought of as cowardly by the rest of the nation's warriors, Getaean archers nonetheless appreciate the safety to be found behind a wall of shields and spears."
#copyspr 201
#rcost 1
#rpcost 10000
#gcost 10011
#weapon 1
#armor 14
#armor 2
#armor 20
#hp 11
#mr 10
#mor 11
#att 10
#def 10
#prec 10
#enc 3
#mapmove 12
#ap 10
#size 2
#formationfighter 2
#end
-- needs a new sprite

#newmonster 5709
#name "Satyr Peltast"
#nametype 109
#copyspr 1532
--#descr "As the nation of Pangaea began to splinter, some of its satyrs followed several Black Dryads to Erebos Woods and swore fealty to Getaea's sorcerer kings. It is said they join Kosonoi in the forests, where together they revel and preserve the old Panic customs. During battle, they wear furs and wield spear and javelin to bring down their foes."
#descr "As the nation of Pangaea began to splinter, some of its satyrs followed several Black Dryads to Erebos Woods and swore fealty to Getaea's sorcerer kings. It is said they join Kosonoi in the forests, where together they revel and preserve the old Panic customs. During battle, they wear furs and wield spear and javelin to bring down their foes."
#rcost 1
#rpcost 10000
#gcost 10012
#weapon 1
#weapon 21
#armor 44
#armor 2
#hp 14
#prot 1
#mr 13
#str 11
#def 11
#prec 10
#enc 3
#mapmove 18
#ap 12
#size 2
#heal
#forestsurvival
#stealthy 0
#maxage 90
#end
-- Needs a new sprite

--I'm not making a new scout monster because Sauro used the default 426 scout.
--I'm leaving 5708 and 5709 unused in case we need to add more units or something.
--I just took those.

#newmonster 5710
#name "Namagyada"
#nametype 136
--#descr "Even Getaea's most basic commanders prefer to ride into combat atop horses. They wield broad swords and wear ring mail armor."
#descr "Young nobles who can trace their lineage back to the nomadic Sauromatian often become leaders of men. The Namagyada has proven himself a capable leader and warrior, and given enough success in battle will eventually become a Nart. They wield sicas and composite shortbows, and wear ring mail armor."
#spr1 "getaea/commander1.tga"
#spr2 "getaea/commander2.tga"
#rcost 1
#rpcost 1000
#gcost 10010
#hp 11
#mr 10
#mor 11
#str 11
#att 10
#def 11
#prec 8
#enc 3
#mapmove 22
#ap 25
#size 3
#ressize 2
#okleader
#command 20
#mounted
#weapon 903
#weapon 596
#weapon 56
#weapon 264
#armor 11
#armor 118
#xpshape 100
#horsetattoo 1
#end
--needs a Nart promotion form

#newmonster 5711
#name "Nart"
#nametype 136
--#descr "Even Getaea's most basic commanders prefer to ride into combat atop horses. They wield broad swords and wear ring mail armor."
#descr "The greatest of the Namagyada, those who have proven themselves many times in battle, are given the title Nart. These warriors are great forces of destruction, said to turn into whirlwinds or gales of wind to dash to their comrades' side during combat. Their skills are exceptional, and their leadership inspires the Getae to fight fearlessly for the Awakening God."
#spr1 "getaea/commander1.tga"
#spr2 "getaea/commander2.tga"
#rcost 1
#rpcost 1000
#gcost 10010
#hp 12
#mr 11
#mor 13
#str 12
#att 11
#def 12
#prec 8
#enc 3
#mapmove 22
#ap 25
#size 3
#ressize 2
#goodleader
#inspirational 1
#mounted
#weapon 903
#weapon 596
#weapon 56
#weapon 264
#armor 11
#armor 118
#horsetattoo 2
#end

#newmonster 5756
#copystats 5703
#name "Dakoi Chieftan"
#nametype 136
--#descr "The Dakoi Chieftans are capable leaders and help whip the Dakoi Warriors into a frenzy when combat starts. They wear armor similar to Dakoi Warriors but do not go berserk in battle. Unlike other Getaean commanders, the Dakoi Chieftans prefer to enter combat on foot." -- I'd potentially like to give this guy a gray beard or something. Make him stand out a little bit.
#descr "Like the Namagyada, Dakoi Chieftans are warriors who have proven themselves in battle. These chieftans are capable leaders who inspire Dakoi Warriors and whip them into a frenzy during combat. Though they wear similar armor to the wolf warriors, Dakoi Chieftans are strategists who must keep their cool and thus do not berserk. Unlike the Namagyada, he prefers to enter combat on foot.
#clearspec
#rpcost 1000
#spr1 "getaea/dakoichief1.tga"
#spr2 "getaea/dakoichief2.tga"
#gcost 10015
#goodleader
#snaketattoo 1
#wolftattoo 1
#forestsurvival
#end

#newmonster 5712
#name "Jade Kyria"
#nametype 135
--#descr "The Jade Kyrie are Cataphracts that strongly excelled in their training. Like the Warrior Priestesses of Sauromatia, the Kyrie are allotted a small measure of religious authority and can cast the most basic of priestly spells."
#descr "While the Getaea are not matriarchal, the occupation of female warriors is established and well-respected. Among the most excellent cataphracts are those known as Jade Kyrie. Much like the warrior priestesses of the past, the Kyrie are granted a small measure of religious authority and can cast the most basic of priestly spells."
#spr1 "getaea/jadekyria1.tga"
#spr2 "getaea/jadekyria2.tga"
#rcost 5
#rpcost 10000
#gcost 130
#weapon 4
#weapon 8
#weapon 56
#armor 12
#armor 2
#armor 118
#hp 13
#mr 13
#mor 14
#str 11
#att 13
#def 14
#prec 8
#enc 3
#mapmove 22
#ap 24
#size 3
#ressize 2
#holy
#younger -20
#female
#magicskill 8 1
#custommagic 25088 20
#expertleader
#mounted
#horsetattoo 2
#end

#newmonster 5713
#name "Vraci"
#nametype 136
#descr "The Vraci are the native casters of Getaea. Sometimes called witch doctors, the Vraci are skilled fire mages and priests. Although they are not the most effective researchers, they heal diseases in the provinces they reside in. Vraci carry an appreciation for life and the elements, and have a particular reverence for fire."
#spr1 "getaea/vraci1.tga"
#spr2 "getaea/vraci2.tga"
--#gcost 180
#rcost 1
#rpcost 1000
#gcost 10010
#weapon 7
#hp 10
#mr 13
#mor 12
#str 10
#att 10
#def 10
#prec 10
#enc 3
#mapmove 14
#ap 12
#fireres 5
#autodishealer 1
#poorleader
--#custommagic 8832 100 -- FWN
--#custommagic 8832 100 -- FWN
--#custommagic 8832 100 -- FWN
--#custommagic 8832 10 -- FWN
--#magicskill 8 2 --h2
#magicskill 0 1
#magicskill 3 1
#custommagic 9344 100
#custommagic 9344 10
#end

#newmonster 5715
--#name "Black Dryad"
#name "Driada Tanar"
#nametype 110
--#descr "Dryads are female satyrs. Once all dryads had white fur on their hindquarters and were blessed with aphrodisiac beauty. Before the vengeful nation of Asphodel was formed, half-men with black hides were exiled from Pangaea. With nowhere to go, several of the Black Dryads found themselves drawn to the Erebos Woods outside of Getaea. There, they saw the decadent revelry of the Kosonoi, as well as their magical aptitude and became enamored with the sorcerer kings. Dryads are respected by the people of Getaea, and although they no longer hold the religious authority they once had in the Sacred Groves, they are sometimes chosen to become the Voice of the Awakening God."
#descr "Dryads are female satyrs. Once all dryads had white fur on their hindquarters and were blessed with aphrodisiac beauty. Driada Tanar are young dryads who have fled the splintering nation of Pangaea. They have found refuge in Getaea's sinister forests, where they preserve old Panic rites and seduce the nation's sorcerer kings. Like their Asphodelian cousins, they no longer posses awe-inspiring beauty, but are dear to the Getic populace and wield small religious authority."
#copyspr 901
#rcost 1
#rpcost 1000
#gcost 10005
#weapon 55
#hp 11
#mr 16
#mor 10
#str 10
#att 10
#def 12
#prec 10
#enc 3
#mapmove 18
#ap 15
#okleader
#age 64
#maxage 90
#magicskill 5 1
#magicskill 6 1
#stealthy 25
#female
#forestsurvival
#heal
#prophetshape 5717
--should they get Pan's randoms on a black dryad chassis?
#end

#newmonster 5716
#name "Koson"
#nametype 136
--#descr "The Kosonoi are sorcerer kings of Getaea that claim to be descended from the Witch Kings of Sauromatia. They no longer practice cannibalism like the Witch Kings, but they are skilled sorcerers and carry authority over the nation's magical affairs. The Kosonoi are allowed minimal religious clout and often steal away into forests at night to partake in blood sacrifice and other dark rituals. They are hedonistic men who dress themselves in gold finery and wear felt hats. The sorcerer kings wield sceptres in place of real weapons, a sign of their status and authority in Getaea."
#descr "The Kosonoi are sorcerer kings of Getaea that claim to be descended from Androphag Witch Kings. They no longer practice cannibalism like their forefathers, but they are skilled sorcerers and carry authority over the nation's magical affairs. The Kosonoi are allowed minimal religious clout and often steal away into forests at night to partake in sacrifice and other dark rituals. They are hedonistic men who dress themselves in gold finery and wear felt hats. The sorcerer kings wield sceptres in place of real weapons, a sign of their status and authority in Getaea."
#spr1 "getaea/koson1.tga"
#spr2 "getaea/koson2.tga"
#rcost 1
#rpcost 1000
#gcost 10015
#hp 14
#mr 16
#mor 16
#str 13
#att 13
#def 13
#prec 8
#enc 3
#mapmove 16
#ap 9
#okleader
#holy
#weapon 446
#maxage 120
#magicskill 8 1
#magicskill 5 2
#magicskill 6 1
#custommagic 12800 100 -- WDN
#custommagic 12800 100 -- WDN
#custommagic 20480 20 -- DB
#older -60
#end

#newmonster 5714
#copystats 5716
#name "Aima Theiko"
#nametype 136
--#descr "The Aimata Theikou are not respected leaders or skilled mages, as their Kosonoi brethren are.  They do have a knack for fortune-telling and interceding with the spirits of their ancestors, and find purpose in this, despite their lack of sheer talent."
--#descr "The Aimata Theikou are neckbeards and failures, but they pretend to be useful on occasion."
#descr "The Aimata Theika are soothsayers of nomadic descent. Unlike the Kosonoi, they are not respected leaders, nor are they particularly skilled mages. However, they have a knack for fortune-telling and communing with their ancestor's spirits. The Aimata share some of the unique proclivities of the Kosonoi, and are known to occasionally indulge in human sacrifice."
#clearmagic
#clearweapons
#clearspec
#maxage 120
#spr1 "getaea/theiko1.tga"
#spr2 "getaea/theiko2.tga"
#rcost 1
#rpcost 40000
#gcost 10015
#hp 13
#str 12
#mr 15
#weapon 9
#magicskill 8 2 --h2
#holy
--#magicskill 5 1
--#magicskill 4 1
#custommagic 6656 10  --S1D1 WSD 1
#onebattlespell "personal luck"
#older -40
#nobadevents 5
#mapmove 16
#end
--FIX ME, the randoms aren't proccing - this is a note from dom4 lol


#newmonster 5717
#copystats 5715
#spr1 "getaea/matriarch1.tga"
#spr2 "getaea/matriarch2.tga"
#name "Driada Matriarch"
#descr "The Driada Matriarch is a Black Dryad who has been chosen as the Voice of the Awakening God. She is well-respected by both commoners and the Kosonoi as a sign of growth and fertility, and plants flourish in the province in which the Matriarch dwells."
#okleader
#inspirational 2
#decscale 3 -- Folks at home, apparently you can have a chassis spread growth, not just a magic site. Pretty nifty stuff!
#end

#newmonster 5758
#copyspr 5702
#name "Tunet lovit"
#cost 1
#rpcost 1
#weapon 903
#armor 12
#armor 2
#armor 16
#size 2
#holy
#onebattlespell luck
#hp 15
#str 12
#mr 13
#att 13
#def 13
#prec 10
#enc 3
#mapmove 16
#AP 10
#OKleader
#inspirational 1
#shockres 15
#descr "The Tunet Lovit, or Thunderstruck, is a man who has cursed the gods and survived their retribution.  Chosen as a champion of the Awakening God, he will lead his god-cursing brethren to glory over the other pretender gods."
#end

---MA Heroes---

#newmonster 5718
#spr1 "getaea/skogu1.tga"
#spr2 "getaea/skogu2.tga"
#name "Returned King"
#descr "After eating his former lover Kirke, Skögu was struck down by her titan father. However, the Man Eater's mastery over death allowed him to return from the Underworld as a powerful wight. Driven from the swamps of his Sauromatian home by Pythian Theurgs, Skögu has arrived in Getaea to find it ruled by the sons of his son. He is now intent on re-educating the soft-hearted Kosonoi and teaching them the forgotten ways of blood magic. In order to accomplish this goal, Skögu has pledged himself to the Awakening God."
#fixedname "Skögu"
#magicskill 5 5
#magicskill 6 1
#magicskill 7 4
#unique
#holy
#size 3
#mounted
#hp 28
#mr 18
#mor 16
#str 18
#att 14
#def 14
#prec 8
#enc 0
#mapmove 24
#ap 16
#expertleader
#maxage 500
#coldres 25
#poisonres 25
#cold 3
#fear 10
#pooramphibian
#undead
#neednoteat
#gcost 0
#weapon 357
#weapon 19
#armor 12
#armor 125
#armor 2
#heroarrivallimit 18
#end

#newmonster 5719
#spr1 "getaea/magissa1.tga"
#spr2 "getaea/magissa2.tga"
#name "Magissa"
--#descr "Raised by an Arcoscephalian Priestess, Bediae claims to be the daughter of a forgotten god. She has an uncanny talent for magic, including an affinity for crafting magic items. When the other Priestesses saw her strange magic, they decried her and banished her from the temples of Arcoscephale. Eventually, she found her way to Getaea, where it is said she taught the Vraci to revere fire and instructed them in the art of healing disease."
#descr "Raised by a Phlegran nun, Bediae claims to be the daughter of an ancient Cyclops. She has an uncanny talent for magic, including an affinity for crafting magic items. When the Trophimoi saw her strange magic, they decried her and banished her from their lands. Eventually, she found her way to Getaea, where it is said she taught the Vraci to revere fire and instructed them in the art of healing disease."
#fixedname "Bediae"
#magicskill 0 2
#magicskill 3 2
#magicskill 6 3
#magicskill 8 2
#unique
#female
#hp 10
#mr 17
#mor 10
#str 9
#att 11
#def 11
#prec 11
#enc 3
#mapmove 16
#ap 8
#poorleader
#maxage 500
#holy
#fixforgebonus 2
#autohealer 1
#weapon 13
#gcost 0
#heroarrivallimit 6
#end

#newmonster 5720
#spr1 "getaea/karya1.tga"
#spr2 "getaea/karya2.tga"
#name "Dryad Crone"
#descr "After the death of her lover Petraios, Karya found herself drawn to the study of necromancy. She poured herself into her craft, hoping to fill the hole that his death had left in her heart. Although she never successfully returned her lover to the land of the living, Karya's devotion to necromancy awakened within her a talent for divining the futures and fates of men. It is said that she foresaw the domestication of Pangaea and the destruction of its forests. Displeased with the apostasy of its beastmen, Karya was the first dryad to leave the splintered nation. She arrived in Erebos Woods and soon found her way to the lands of Getaea. Several Black Dryads followed closely behind her, seeking refuge that Asphodel could not provide. Karya took it upon herself to watch over her dark-coated sisters, and in order to do so has pledged fealty to the Awakening God."
#fixedname "Karya"
#magicskill 2 1
#magicskill 3 1
#magicskill 5 3
#magicskill 6 3
#hp 11
#mr 17
#mor 13
#str 10
#att 11
#def 13
#prec 10
#enc 3
#mapmove 18
#ap 15
#poorleader
#unique
#female
#forestsurvival
#heal
#stealthy 25
#maxage 90
#startage 236
#beastmaster 2
#nobadevents 10
#gcost 0
#weapon 7
#weapon 55
#heroarrivallimit 6
#end

---MA Misc Units---

#newmonster 5721
#spr1 "getaea/theophag1.tga"
#spr2 "getaea/theophag2.tga"
#name "Theophag"
#descr "During the onslaught of the Ashen Empire, Skögu's Witch King sons retreated into the Pythian Swamps and through its gate to the Underworld. The Theophag is a prodigal son who instead chose to lead the remaining Sauromatian tribes into the land of Getaea. There, he took many wives and his sons ruled the nation as sorcerer kings. Having successfully become God-King among his people, the Theophag has risen to the role of a Pretender God. He has inherited the insatiable hunger of his father and now seeks to feed on the flesh of the false Pretenders."
#hp 16
#mr 18
#mor 30
#str 14
#att 14
#def 14
#prec 8
#enc 3
#mapmove 18
#ap 16
#expertleader
#magicskill 5 1
#magicskill 6 1
#magicskill 7 1
#maxage 500
#mounted
#swampsurvival
#fear 5
#popkill 2
#incunrest 30
#gcost 9990
#pathcost 20
#startdom 1
#weapon 357
#weapon 494
#armor 12
#armor 125
#armor 2
#end

--This is Cliff's beautiful summon!
#newmonster 5722
#spr1 "getaea/solomonar1.tga"
#spr2 "getaea/solomonar2.tga"
#name "Solomonar"
#descr "The Solomonari are powerful air wizards, able to control clouds and the rain. They ride fearsome cloud dragons into combat, and strike out at enemies with whips made of lightning. They are loyal to their brother Solomonari and would sooner die than divulge their order's secrets. Beyond that, the individual mages are free to lend their help to whomever they choose to. Once his fealty has been negotiated, a Solomonar is a reliable ally who will fight against the enemies of the Awakening God."
#gcost 255
#rcost 1
#rpcost 10000
#hp 10
#mr 15
#mor 13
#str 10
#att 10
#def 10
#prec 10
#enc 3
#mapmove 22
#researchbonus -5
#slowrec --I may change my mind on this later, or turn it back into a summon
#ap 15
#poorleader
#slowrec
#startage 35
#maxage 50
#mounted
#flying
#stormimmune
#fear 5
#stormpower 2
#shapechange 5723
#size 5
#nametype 136
#itemslots 13446
#weapon 208
#weapon 902
#magicskill 1 3
#magicskill 2 1
#custommagic 1792 100 -- AEW
--As the Solomonari have been described as red-haired giants (Romanian: uriaşi, pl.), a connection to them and the legendary Red Jews (evreilor roşii[g]) has been suggested by Adrian Majuru t. wikipedia article on solomonari
--if needed as a balance change they could be giants
--if path access becomes an issue these will become non-sacredsummons who are giants
#end

#newmonster 5723
#copyspr 1565
#name "Beggar"
#descr "Despite their rather fickle kings, the getic people are often known to act charitably towards beggars. This is not entirely because they are of excellent moral character, however -it is rumored among the getic people that such beggars may be Solomonari in disguise, and if scorned will summon dragons to eat those they deem wicked."
#rcost 1
#rpcost 10260
#hp 10
#mr 15
#mor 13
#str 10
#att 10
#def 10
#prec 10
#enc 3
#mapmove 12
#ap 12
#researchbonus -5
#shapechange 5722
#end

#newmonster 5759
#name "Driada Batran"
#descr "Dryads are female satyrs. Once all dryads had white fur on their hindquarters and were blessed with aphrodisiac beauty. Driada Batran are wizened crones, leaders among Getaea's beastmen. They lurk deep within Erebos Woods, where they preserve old Panic rites and seduce the nation's sorcerer kings. Like their Asphodelian cousins, they no longer posses awe-inspiring beauty, but are dear to the Getic populace and wield religious authority."
#nametype 110
#copyspr 901
#rcost 1
#rpcost 1000
#gcost 10005
#weapon 55
#hp 11
#mr 17
#mor 10
#str 10
#att 10
#def 12
#prec 10
#enc 3
#mapmove 18
#ap 15
#badleader
#startagemodifier 75
#maxage 50
#magicskill 5 1
#magicskill 6 2
#magicskill 8 2
#stealthy 25
#female
#forestsurvival
#heal
#prophetshape 5717
--should they get Pan's randoms on a black dryad chassis?
#end

---LA Units---

#newmonster 5725
#name "Getaean Infantry"
#nametype 146
#descr "Getaea's infantry is its first line of defense against both invaders and the sinister monsters that stalk its dark forests. They are armed with spears and javelins, and protect themselves with ring mail and shields."
#copyspr 201
#rcost 1
#gcost 10010
#rpcost 10000
#hp 10
#mr 10
#mor 10
#str 10
#att 10
#def 10
#prec 10
#enc 3
#mapmove 16
#ap 12
#weapon1
#weapon21
#armor 11
#armor 2
#armor 20
#end

#newmonster 5726
#name "Getaean Falxman"
#nametype 146
#descr "Falxmen carry long polearms tipped with sinister, curved blades. These weapons are heavy and preclude the use of shields, but are devastatingly effective against lightly armored foes. Enemies are often peppered with a volley of javelins or arrows before Falxmen chase down and hack apart the survivors."
#copyspr 2109
#rcost 1
#rpcost 10000
#gcost 10010
#hp 10
#mr 10
#mor 10
#str 10
#att 10
#def 10
#prec 10
#enc 3
#mapmove 16
#ap 12
#weapon 904
#armor 12
#armor 20
#end

#newmonster 5727
#name "Ticalos"
#nametype 109
#descr "When Asphodelian Black Dryads arrived in Erebos Woods, they brought a number of satyr sneaks with them. The Ticalosi of Getaea are descendants of such beastmen. Even among their kin, the Ticalosi are infamous as troublemakers and philanderers. They are selfish creatures, equally liable to seduce wives and daughters at home or afar. Ticalosi cause unrest in the province where they dwell."
#copyspr 227
#rcost 1
#rpcost 10000
#gcost 10009
#hp 12
#mr 13
#mor 9
#str 11
#att 10
#def 12
#prec 10
#enc 3
#mapmove 18
#ap 14
#weapon 1
#armor 44
#maxage 90
#stealthy 20
#incunrest 5
#undisciplined
#forestsurvival
#heal
#end

#newmonster 5728
#name "Vanator"
#nametype 109
#descr "Sometimes disparagingly called Vanatori de fuste, the Vanatori are the hunters of Getaea. They are more disciplined than their Ticalosi brethren, and wield spears and short bows in battle. In order to bring down larger game, the Vanatori sometimes organize hunting parties with the Lupoaice. Successful hunts are said to bring good luck, and a portion of the spoils are always left in the forest to appease its spirits."
#copyspr 1532
#rcost 1
#rpcost 10000
#gcost 10013
#hp 14
#mr 13
#mor 10
#str 11
#att 11
#def 13
#prec 10
#enc 3
#mapmove 18
#ap 14
#weapon 1
#weapon 23
#armor 44
#maxage 90
#stealthy 0
#forestsurvival
#heal
#end

#newmonster 5729
#name "Desfranat"
#nametype 109
#descr "On the rare occasion that a Lupoaica gives birth to a son, the child is said to be imbued with the spirit of the forest. Such satyrs are called Desfranati by Getaeans. In combat, they wield mighty battleaxes and lose themselves to reckless battle lust. Desfranati are filled with an unrivaled appreciation for life and its pleasures, and cause unrest in the province where they dwell."
#copyspr 708
#rcost 1
#rpcost 10000
#gcost 10013
#hp 15
#prot 0
#mr 13
#mor 12
#str 12
#att 12
#def 13
#prec 10
#enc 3
#mapmove 18
#ap 15
#weapon 18
#weapon 55
#armor 44
#maxage 90
#forestsurvival
#heal
#stealthy 0
#berserk 3
#incunrest 5
#end

#newmonster 5730
#name "Calaret"
#nametype 146
#descr "Archery has fallen to the wayside and is now only practiced by the Calareti and Vanatori. The Calareti are human horsemen armed with short swords and composite bows. They wear ring hauberks and half helms to protect themselves in battle. While they are not as effective at pillaging provinces as the Sauromatian raiders of yore, they fill an important role in Getaean armies."
#copyspr 137
#rcost 5
#rpcost 10000
#gcost 10015
#hp 11
#mr 10
#mor 11
#str 11
#att 10
#def 11
#prec 10
#enc 3
#mapmove 22
#ap 26
#size 3
#ressize 2
#mounted
#weapon 6
#weapon 264
#armor 12
#armor 118
#armor 1
#end

#newmonster 5731
#name "Heavy Calaret"
#nametype 146
#descr "Heavy Calareti are the more-heavily armored counterpart to the Calareti. They forgo the use of bows and instead wield lances and shields against their enemies. Instead of ring mail, they wear heavier scale armor into battle. Many of the Heavy Calareti are drafted from the ranks of farmers and metalworkers, and are therefore not as skilled in battle as the Cavaleri."
#copyspr 20
#rcost 10
#rpcost 10000
#gcost 10015
#hp 11
#mr 10
#mor 11
#str 11
#att 10
#def 11
#prec 10
#enc 3
#mapmove 22
#ap 26
#size 3
#ressize 2
#mounted
#weapon 4
#weapon 903
#armor 12
#armor 118
#armor 2
#end

#newmonster 5732
#name "Cavaler"
#nametype 146
#descr "The Cavaleri are drafted from the aristocracy of Getaea. They are proud and well-trained warriors who carry on the tradition of their ancestors. Because of their status, they can afford better arms and armor than commoners can. The Cavaleri are the closest thing Getaea has to knights, but they are not so disillusioned to allow honor impede with combat. They wield lances and sica against their foes, and protect themselves with scale armor and shields."
#copyspr 1553
#rcost 10
#rpcost 10000
#gcost 10020
#hp 12
#mr 11
#mor 13
#str 11
#att 11
#def 12
#prec 10
#enc 3
#mapmove 22
#ap 24
#size 3
#ressize 2
#mounted
#weapon 4
#weapon 903
#weapon 56
#armor 9
#armor 118
#armor 2
#end

#newmonster 5733
#name "Lupoaica"
#nametype 135
#descr "Female orphans who are deemed unworthy for blood sacrifice are usually ceded to Getaean temples. They are raised as Lupoaice, or she-wolves. When they have reached adulthood, they are anointed with Stygian water as part of a sacred ritual. This is symbolic of the Stygian Wolf, a creature revered by the people of Getaea. As both priestess and huntress, it is a Lupoaica's obligation to protect and nourish her people in times of need. Lupoaice take this duty seriously, and are known to grace Getaea's citizens with rations or warmth during cold nights. In combat, the Lupoaice wield spear and shield against their enemies. They are armored only with wolf pelts, but the Stygian magic of their inauguration has rendered them resistant to most physical harm."
#copyspr 51
#rcost 1
#rpcost 10000
#gcost 10016
#hp 12
#prot 5
#mr 14
#mor 13
#str 11
#att 12
#def 12
#prec 10
#enc 3
#mapmove 18
#ap 12
#weapon 1
#armor 2
#armor 44
#armor 120
#holy
#spiritsight
#forestsurvival
#invulnerable 15
#female
#end

#newmonster 5734
#name "Satyr Scout"
#nametype 109
#descr "With the satyr population of Getaea increasing, the task of scouting has been relegated to the naturally stealthy goatmen. They can lead small raiding parties and wield composite bows when they must enter combat. Like the satyrs of Asphodel, the wounds of Getaean satyrs heal over time."
#copyspr 228
#rcost 1
#rpcost 10000
#gcost 10013
#hp 15
#mr 13
#mor 11
#str 12
#att 12
#def 12
#prec 10
#enc 3
#mapmove 18
#ap 15
#weapon 1
#weapon 264
#armor 44
#stealthy 20
#maxage 90
#forestsurvival
#heal
#poorleader
#end

#newmonster 5735
#name "Saragea"
#nametype 146
#descr "The native tribes of Getaea have mostly assimilated, but a few individuals still remember the old ways. The Saragea are named after the chieftans of yore, but hold minimal clout outside of combat. They lead the armies of Getaea into battle, spurring human and satyr alike to fight for the glory of the Awakening God."
#copyspr 136
#rcost 10
#rpcost 10000
#gcost 10020
#hp 12
#mr 10
#mor 13
#str 11
#att 12
#def 12
#prec 10
#enc 3
#mapmove 22
#ap 26
#size 3
#ressize 2
#mounted
#weapon 4
#weapon 903
#weapon 56
#armor 12
#armor 118
#armor 2
#goodleader
#end

#newmonster 5736
#name "Lupoaica Blajina"
#nametype 135
#descr "Lupoaice who excel in their training are known as Lupoaice Blajine, or gentle she-wolves. They are formally anointed as priestesses and, unlike the lesser Lupoaice, can cast the most basic of priestly spells. Lupoaice Blajine are talented leaders, and are frequently tasked with leading Getaean humans and satyrs into battle. As symbols of nurture and refuge, they are sometimes chosen to become the voice of the Voice of the Awakening God."
#copyspr 51
#rcost 1
#rpcost 10000
#gcost 10020
#hp 13
#prot 6
#mr 15
#mor 14
#str 11
#att 12
#def 13
#prec 10
#enc 3
#mapmove 18
#ap 12
#weapon 1
#armor 2
#armor 44
#armor 120
#holy
#forestsurvival
#invulnerable 15
#female
#spiritsight
#magicskill 8 1
#goodleader
#prophetshape 5743
#end

#newmonster 5737
#name "Stonemason" -- Oh queso this asshole needs different/better armor and weapons and stuff.  -- also his magic may warrant an update
#nametype 146
#descr "Getaea's Stonemasons were once human wizards known as Solomonari. The Scorpie Queens eventually grew jealous of their magical skill and persecuted them. Rather than face the queens' stormy wrath, the Solomonari chose to drop their craft and now serve the Awakening God as masons and smiths. While they are no longer such powerful wizards, they retain the secrets of the Solomonari and their knowledge is highly prized. As masons, they are skilled and can help defend a castle under siege."
#copyspr 325
#rcost 1
#rpcost 10000
#gcost 10010
#hp 12
#mr 14
#mor 13
#str 11
#att 10
#def 10
#prec 10
#enc 3
#mapmove 14
#ap 12
#older 10
#mason
#castledef 20
#siegebonus 20
#poorleader
#magicskill 3 1
#custommagic 1280 10
#weapon 13
#armor 158
#end

#newmonster 5738
#name "Calugar"
#nametype 109
#descr "The Calugari are descended from Asphodelian satyrs who sought refuge alongside the Scorpii's Black Dryad mothers. With the deposing of the Kosonoi and their Aimata brothers, these satyrs have inherited the nation's religious duties. They are fond of women and wine, and dress themselves with gold finery not unlike their predecessors. The Calugari frequently whittle away their time in forest temples where they sacrifice to the Awakening God and corrupt young Scorpie princesses."
#copyspr 1881
#rcost 1
#rpcost 10000
#gcost 10013
#hp 15
#mr 13
#mor 11
#str 12
#att 12
#def 12
#prec 10
#enc 3
#mapmove 16
#ap 15
#holy
#magicskill 8 2
#stealthy 0
#okleader
#weapon 92
#weapon 55
#maxage 90
#older -10
#forestsurvival
#end

#newmonster 5739
#name "Valva Padurii"
#nametype 147
#descr "FILLER"
#copyspr 280
#rcost 1
#rpcost 10000
#gcost 10030
#hp 13
#mr 15
#mor 10
#str 9
#att 10
#def 11
#prec 10
#enc 2
#mapmove 16
#ap 12
#magicbeing
#maxage 500
#magicskill 5 1
#magicskill 6 1
#custommagic 12800 100 -- this should be reconsidered
#stealthy 10
#forestsurvival
#female
#poorleader
#shapechange 5740
#weapon 92
#end

#newmonster 5740
#name "Horned Owl"
#nametype 144
#descr "FILLER"
#copyspr 2092
#hp 7
#mr 15
#mor 8
#str 6
#att 8
#def 11
#prec 8
#enc 3
#mapmove 22
#ap 7
#magicbeing
#maxage 500
#flying
#animal
#stealthy 20
#forestsurvival
#female
#poorleader
#shapechange 5739
#weapon 29
#end

#newmonster 5741
#name "Scorpie"
#nametype 110
#spr1 "getaea/princess1.tga"
#spr2 "getaea/princess2.tga"
#descr "The Scorpii are the ruling class of Getaea, the sorceress daughters of the last era's Kosonoi. Due to their horns and cloven hooves, the Scorpii are sometimes rumored to be of fiendish heritage. In truth, however, they are the daughters of Asphodelian Black Dryads. The Scorpii eventually resorted to sinister magic to restore the daimonic beauty their mothers had once lost. The most powerful of dryads rose to become Getaea's queens. The less powerful Scorpii, sometimes known as Scorpie Princesses, are still learning the powerful magic of their Witch King ancestors. At night, the princesses steal away into Getaea's dark forests, where they often make merry with the lecherous Calugari."
#rcost 1
#rpcost 10000
#gcost 10020
#hp 12
#mr 16
#mor 13
#str 10
#att 12
#def 12
#prec 10
#enc 3
#mapmove 18
#ap 15
#stealthy 15
#awe 3
#heal
#female
#maxage 90
#older -35
#forestsurvival
#poorleader
#magicskill 7 1
#custommagic 28672 100
#weapon 55
#end

#newmonster 5742
#name "Scorpie Queen"
#nametype 110
#spr1 "getaea/queen1.tga"
#spr2 "getaea/queen2.tga"
#descr "The Scorpii are the ruling class of Getaea, having replaced the Kosonoi of old. Due to their horns and cloven hooves, the Scorpii are sometimes rumored to be of fiendish heritage. In truth, however, they are the daughters of Asphodelian Black Dryads. The Scorpii eventually resorted to sinister magic to restore the daimonic beauty their mothers had once lost. Scorpie Queens are powerful sorceresses skilled primarily in blood magic, and have become particularly enamored with the taste and color of blood. They have been tainted by the bloody rituals that restored their beauty and now indulge in sacrifice and hedonism."
#rcost 1
#rpcost 40000
#gcost 10025
#slowrec
#hp 12
#mr 17
#mor 15
#str 11
#att 12
#def 13
#prec 10
#enc 3
#mapmove 18
#ap 15
#stealthy 20
#awe 4
#heal
#female
#maxage 90
#older -70
#forestsurvival
#okleader
#magicskill 5 2
#magicskill 6 1
#magicskill 7 2
#custommagic 25088 100
#custommagic 20480 10
#weapon 55
#end

#newmonster 5743
#name "Lupoaica Alfa"
#nametype 135
#descr "The Lupoaica Alfa is a Lupoaica Blajina who has been chosen as the Voice of the Awakening God. She is the gentlest and most mild of the Lupoaice, and her very presence in a province will reduce unrest. The Lupoaica Alfa is sacred to beasts, which thus have a difficult time striking her."
#copyspr 51
#rcost 1
#rpcost 10000
#gcost 10020
#hp 13
#prot 7
#mr 17
#mor 14
#str 12
#att 13
#def 14
#prec 10
#enc 3
#mapmove 18
#ap 12
#weapon 1
#armor 2
#armor 176
#holy
#forestsurvival
#invulnerable 15
#spiritsight
#animalawe 1
#incunrest -30
#female
#expertleader
#end

---LA Heroes---

#newmonster 5744
#name "Fiu Lupesc"
#fixedname "Rasuil"
#descr "Rasuil is the firstborn of the last great Getaean King, Decebal, who was the first Koson to take a Black Dryad as his wife. The Getaean prince grew up to be a great sorcerer and warrior, much like Decebal himself. When he eventually saw the Kosonoi deposed by Scorpie Queens, Rasuil raged at his mother, who banished him from the capital. The Getaean prince found his way to Erebos Woods, where inside of an ancient ash tree he found a portal to the Underworld. He passed through it and into the waters of the river Styx, seeking the advice of his Witch King ancestors. Rasuil was caught up in its brackish waters, but managed to haul himself to the safety of a nearby shore. There, he found himself face-to-face with a pack of wolves colored midnight by the Stygian waters. The beasts recognized the prince's sorcerous talent and accepted him as kin. When Rasuil finally returned to the capital, it was at the head of an army of Stygian wolves. The spurned prince used his sorcery to open a portal to Kokytos, where he sealed his spiteful mother in its icy depths. Since then, Rasuil has wandered the country, seeking glory and renown. With the arrival of the Awakening God, he has pledged his service and seeks to redeem his father in the eyes of his people. Rasuil is sacred to Stygian wolves, and is always accompanied by several in battle."
#copyspr 5716
#unique
#holy
#hp 18
#mr 17
#mor 16
#str 13
#att 13
#def 13
#prec 8
#enc 2
#mapmove 16
#ap 13
#forestsurvival
#goodleader
#maxage 200
#startage 73
#fear 5
#beastmaster 2
#batstartsum2 5750
#invulnerable 15
#magicskill 2 2
#magicskill 5 4
#weapon 92
#heal
#heroarrivallimit 12
#gcost 0
#end

#newmonster 5745
#name "Tainted Queen"
#fixedname "Igrat"
#spr1 "getaea/igrat1.tga"
#spr2 "getaea/igrat2.tga"
#descr "Igrat is the sister of Rasuil, the third child of a powerful dryad matron by the name of Mahlat. Unlike most black dryads, Mahlat was beautiful, and easily seduced Decebal. When she was grown, Igrat learned both the art of seduction and the forbidden practice of blood magic from her mother. Igrat taught the young Scorpie princesses the tainted rituals that could restore their beauty, and in doing so kindled in them a thirst for blood. It is said that Igrat was born as a manifestation of the winds, and has fury and passion befitting such a disposition. Like her mother, the Tainted Queen is spiteful and never forgets those who have scorned her. Seeking new enemies to torment, Igrat has allied herself with the Awakening God."
#unique
#hp 16
#mr 17
#mor 15
#str 12
#att 12
#def 12
#prec 10
#enc 2
#mapmove 18
#ap 13
#forestsurvival
#okleader
#maxage 200
#startage 52
#stealthy 25
#awe 4
#seduce 9
#female
#weapon 55
#magicskill 1 2
#magicskill 6 1
#magicskill 7 4
#heal
#gcost 0
#heroarrivallimit 12
#end

#newmonster 5746
#name "Unblemished"
#fixedname "Lemnica"
#spr1 "getaea/lemnica1.tga"
#spr2 "getaea/lemnica2.tga"
#descr "Lemnica is a beautiful dryad who is rumored to be the first legitimate Scorpie. It is whispered by heretics and soothsayers that Igrat's mother was not a dryad, but a powerful witch with the lower part of a hind. Igrat and her sister have dismissed such suppositions, and are still widely known as the first of the Scorpii. Lemnica chose not to partake in the grisly rituals that restored beauty to the black dryads, instead exiling herself from the capital. For years, she resided in C'tis where it is said she studied under its Sauromancers and became a powerful mage. Lemnica is a kind soul, and despite having abstained from Igrat's bloody rituals, time has restored a small measure of her daimonic beauty. Although she was well-respected and revered in the halls of C'tis, Lemnica has heard the call of the Awakening God and finally returned to the lands of Getaea that she once called home."
#unique
#hp 12
#mr 17
#mor 15
#str 11
#att 12
#def 13
#prec 10
#enc 3
#mapmove 18
#ap 15
#forestsurvival
#female
#maxage 90
#older -70
#stealthy 65
#awe 4
#seduce 9
#okleader
#weapon 55
#magicskill 0 2
#magicskill 5 1
#magicskill 6 3
#heal
#gcost 0
#heroarrivallimit 12
#end

#newmonster 5747
#name "Panic Lytrate"
#copyspr 709
#nametype 109
#descr "After the fall of Asphodel, the remaining Panic Apostates wandered without purpose. With their once-great god now dead and defeated, they refused to seek sanctum with their Pangaean brothers who had forgotten the ways of the wild. One by one, the Apostates found their way to the mystical Erebos Woods and the Getaean kingdom beyond. Now ruled by the Scorpie daughters of Asphodelian dryads, Getaea became the Apostates' haven. There, they have repented and serve the Awakening God as the reborn form of the once-great Dark Lord."
#hp 27
#prot 4
#mr 17
#mor 16
#str 16
#att 9
#def 9
#prec 9
#enc 2
#mapmove 18
#ap 15
#goodleader
#size 3
#weapon 7
#maxage 150
#stealthy 0
#animalawe 1
#beastmaster 1
#forestsurvival
#heal
#magicskill 5 2
#magicskill 6 3
#custommagic 13824 100 -- WEDN
#gcost 0
#heroarrivallimit 6
#end

---LA Misc Units---

#newmonster 5748
#name "Aksei Driada"
#fixedname "Finaskelis"
#spr1 "getaea/aksei1.tga"
#spr2 "getaea/aksei2.tga"
#descr "The Aksei Driada is a powerful sorceress who claims to be the firstborn of the Asphodelian Scorpii. She appears to have inherited the powerful hunger of her father's Man Eater ancestor, and has become enamored with the taste of blood. The Aksei Driada seeks to cover the civilized world in the blood of innocents, and her fury is terrifying to behold. She is a queen among queens, and with her enormous sorcerous power she has donned a godly mantle and assumed the role of a Pretender God."
#hp 16
#mr 18
#str 12
#att 12
#def 12
#prec 10
#enc 2
#mapmove 18
#ap 13
#goodleader
#magicskill 2 1
#magicskill 6 1
#magicskill 7 1
#forestsurvival
#female
#weapon 55
#heal
#maxage 200
#startage 58
#fear 5
#popkill 1
#incunrest 20
#gcost 9990
#pathcost 20
#startdom 1
--revisit cost later, this is way too cheap
#end

#newmonster 5749
#name "Urias"
#spr1 "getaea/urias1.tga"
#spr2 "getaea/urias2.tga"
#nametype 109
#descr "While the birth of a minotaur calf with a white hide is highly celebrated in Pangaea, the opposite is equally true. When the first black minotaurs were born, they were seen as dire portents and the calves were immediately banished. The black minotaurs wandered a long time before finding Erebos Woods. There, they nourished themselves on fruit and nectar, growing to enormous proportions. Now, they are known to the Getaean people as Uriasi, or giants. They are usually peaceful and can only be bound to service through a special ritual cast by a nature mage. When their ire is eventually roused, the black-hided Uriasi abandon all sense of self-preservation and throw themselves into terrifying, destructive frenzies."
#hp 37
#prot 10
#mr 14
#mor 15
#str 22
#att 12
#def 10
#prec 8
#size 4
#enc 3
#mapmove 18
#ap 18
#maxage 300
#gcost 0
#berserk 5
#trample
#forestsurvival
#heal
#weapon 331
#weapon 55
#end

#newmonster 5750
#name "Stygian Wolf" -- Considering giving this unit fear, although that might could be OP.  PS awoo
#copyspr 1224
#nametype 144
#descr "Wolves that stumbled into the Stygian depths and flourished against all odds, these jet-black animals have imbibed and bathed in the Stygian waters.  This has made them sensitive to sorceries and made their hides nearly impenetrable by mundane weaponry.  This has made them nearly fearless of anything mortal, making them a valuable addition to the armies of the Awakening God." --needs editing
#hp 25
#prot 7
#mr 15 -- Cliff thinks this might be too high.
#mor 14
#str 15
#att 13
#def 11
#prec 5
#size 3
#enc 2
#mapmove 18
#ap 28
#maxage 45
#gcost 0
#coldres 15
#darkvision 100
#spiritsight
#stealthy 40
#animal
#weapon 20
#holy
#forestsurvival
#invulnerable 15 -- Cliff thinks this might be too low.
#end

#newmonster 5751
#name "Pricolici"
#copyspr 633
#nametype 105
#descr "A Pricolici is a violent and sadistic criminal reanimated by dark magic. Though it was once man, the Pricolici now has wolven semblance, and a hunger to match. They are sometimes confused with werewolves, but they are undead and possess an aura of fear that their living cousins lack. Pricolici are most commonly used by the Valve Padurii to target and kill their enemies."
#hp 32
#prot 8
#mr 16
#mor 30
#str 16
#att 13
#def 12
#prec 8
#size 3
#enc 0
#mapmove 14
#ap 14
#maxage 500
#gcost 0
#coldres 15
#poisonres 25
#undead
#spiritsight
#weapon 20
#weapon 29
#weapon 29
#regeneration 10
#forestsurvival
#neednoteat
#fear 5
#end

#newmonster 5752
#name "Strigoi"
#copyspr 181
#descr "The Strigoi are restless spirits said to have once been Sauromatian Witch Kings in life. They were powerful sorcerers, both respected and feared by the tribes they ruled. During the onslaught of the Ashen Empire, the Witch Kings retreated into the Pythian Swamps and through the gate to its Stygian Underworld. After such a long time spent in the land of the dead, the Strigoi have become mere glimmers of what they once were in life. They yearn to redeem themselves in combat and spill the blood of the Awakening God's enemies. Strigoi can only be given physical form by a ritual cast by a suitably powerful death mage. They are immortal and will return to Erebos Woods if slain in battle. The Strigoi are both dreadful warriors and accomplished sorcerers. Though they have forgotten the nature magic they once knew in life, the Stygian waters have imprinted on them the magics of water and ice."
#hp 33
#prot 8
#mr 17
#mor 30
#str 15
#att 16
#def 16
#prec 8
#enc 0
#mapmove 26
#ap 13
#maxage 1000
#coldres 25
#poisonres 25
#cold 3
#fear 5
#summon1 566
#undead
#immortal
#ethereal
#spiritsight
#amphibian
#neednoteat
#deathcurse
#gcost 0
#weapon 41
#armor 14
#armor 21
#magicskill 2 1
#magicskill 5 4
#magicskill 7 1
#custommagic 16896 100
#end

#newmonster 5753
#name "Sfint"
#copyspr 1745
#descr "Sometimes called Saints, the Sfinti were once messengers of the previous Pantokrator. They eventually grew tired of his yoke and sought to betray him. When one of the Pantokrator's generals informed him, he banished the Sfinti from his divine realm. The sky was resealed before they could fall to the depths of hell, freezing them between the two. Now the Sfinti stalk the earth, intercepting and devouring the souls of righteous men. They have grown fat and brutish on their diet of souls, and their wings can no longer carry their bulk. Sfinti may be bound with the sacrifice of human blood, and are sacred to the people of Getaea."
#hp 30
#prot 4
#mr 17
#mor 30
#str 20
#att 13
#def 10
#prec 9
#enc 2
#mapmove 18
#ap 12
#maxage 1000
#neednoteat
#demon
#holy
#size 3
#gcost 0
#supplybonus -3
#weapon 461
#end

newmonster 5754
name "Capcaun"  --Based on the Serbian Psoglav and the Romanian Capcaun.
spr1 "getaea/capcaun1.tga"
spr2 "getaea/capcaun2.tga"
descr "The Capcauni are ancient panic beings that were cursed by the previous Pantokrator when they adopted the practice of cannibalism. They are hideous ogres with gruesome appetites, and they make their homes in the Getaean mountains. Like the panic creature it once was, the capcaun has a human torso and the legs of a hoofed beast, but being cursed has replaced his majestic rams head with that of a hungry dog. It is an evil, cunning beast that kidnaps and feeds on children and young maidens. They retain the earth and nature magic of the ancient panii of Gaea's sacred groves, and their diet of human flesh has made them accomplished blood mages as well. Capcauni have a gruesome appetite and must feed on a given amount of people every month, thus sowing unrest and misery in the province they are located in."
#hp 27
#size 4
#prot 5
#mr 17
#mor 16
#str 19
#att 13
#def 10
#prec 9
#enc 3
#mapmove 18
#ap 15
#maxage 150
#okleader
#weapon 20
#weapon 29
#weapon 29
#gcost 0
#stealthy 0
#forestsurvival
#mountainsurvival
#heal
#fear 5
#popkill 2
#supplybonus -8
#incunrest 30
#magicskill 3 1
#magicskill 6 3
#magicskill 7 2
#custommagic 25600 1
#end

=====New Sites=====

#newsite 1615
#name "Godskull Cauldron"
#path 5 -- Death
#level 0
#rarity 5
#gems 5 3
#homecom 5716
#end

--MA Erebos Woods
#newsite 1616
#name "Erebos Woods"
#path 6 -- Nature
#level 0
#rarity 5
#gems 2 1 -- Water
#gems 3 1 -- Earth
#homemon 5707
#end

--#newsite 1617
--#name "Erebos Woods"
--#path 6
--#level 0
--#rarity 5
--#gems 6 1
--#com 5715 -- Black Dryad
--#end

--This is only kept out of sentimentality.  We originally planned on Satyrs only being available in some provinces, but had issues making that work properly.
#newsite 1619
#name "Satyr Encampment"
#path 6
#level 0
#rarity 5
#com 2118
#mon 1532
#incscale 0
#end

--This is the LA Erebos Woods.
#newsite 1620
#name "The Erebos Woods"
#path 6
#level 0
#rarity 5
#gems 3 1  --Earth
#gems 5 2  --Death
#gems 6 1  --Nature.
#end

#newsite 1621
#name "Mahlic Altar" --Edit maybe
#path 7 -- Blood
#level 0
#rarity 5
#gems 7 1
#homemon 5733
#homecom 5742
#end

=====Nation Data=====

#selectnation 125
#clearnation
#name "Getaea"
#epithet "Capricious Kings"
#era 2
--#descr "Getaea was once a fledgling Arcoscephalian colony established amid the barbaric Dakos tribes. After its mother empire began to wane, the nation was invaded by Sauromatian warriors. It is now a land ruled by Kosonoi said to be descended from the powerful Androphag Witch Kings. Although they no longer practice cannibalism, the Kosonoi are hedonistic men and accomplished sorcerers who hold sacrifices in the name of the Awakening God. The dark magic of Getaea's sorcerer kings has recently attracted the attention of refugee beastmen from the splintered nation of Pangaea, who have only served to exacerbate the problem. Getaea has become a conflicted nation, held between the practice of pagan revelry and the advance of civilization."
#descr "After the fall of Sauromatia, one of Skögu's sons led his people to the lands of the barbaric Dakoi. A new nation, known as Getaea, was the result. The nation is ruled by Kosonoi said to be descended from the powerful Androphag Witch Kings. Although they no longer practice cannibalism, the Kosonoi are hedonistic men and accomplished sorcerers who hold sacrifices in the name of the Awakening God. The dubious practices of Getaea's sorcerer kings has recently attracted the attention of refugee beastmen from the splintered nation of Pangaea, who make their home in Erebos Woods. Getaea has become a conflicted nation, held taut between the practice of pagan revelry and the forward march of civilization."

#summary "Race: Humans and a handful of satyrs.

Military: Infantry, elite medium cavalry, Dakoi berserkers, along with Arcoscephale-style Hoplites recruitable from coastal provinces.

Magic: Strong death magic, and strong air magic available from mountains.  Weak fire, earth, water, and nature magic.

Priests: Average."
#brief "Getaea was reformed from an Arcoscephalian colony after it was invaded by Sauromatian tribes. Its military uses infantry, cavalry and native Dakoi Warriors, and its mages are powerful sorcerer kings. Getaea has recently become a refuge for some Asphodelian dryads."
#color 0.45 0.37 0.27
#secondarycolor 0.28 0.0 0.38
#flag "getaea/flagma.tga"
#startsite "Godskull Cauldron"
#startsite "Erebos Woods"
#cavenation 0
#likesterr 128
#clearrec
#startcom 5710
#startscout 426
#startunittype1 5701
#startunitnbrs1 15
#startunittype2 5702
#startunitnbrs2 15
#addrecunit 5700
#addrecunit 5701
#addrecunit 5702
#addrecunit 5703
#addrecunit 5704
#addrecunit 5705
#addrecunit 5706
#coastunit1 5708
#forestrec 5709
#addreccom 426
#addreccom 5710
#addreccom 5712
#addreccom 5713
#addreccom 5714
#forestcom 5715
#mountaincom 5722
#hero1 5718
#hero2 5719
#hero3 5720
#defcom1 5710
#defcom2 5712
#defunit1 5701
#defunit1b 5700
#defunit2 5702
#defmult1 10
#defmult1b 10
#defmult2 20
#defmult2b 0
#likespop 42
#likespop 78
#cleargods
#homerealm 3
#homerealm 10
#addgod "Theophag"
#cheapgod20 "Theophag"
#addgod "Idol of Sorcery"
#addgod "Idol of Beasts"
#addgod "Volla of the Bountiful Forest"
#addgod "Horned One"
#addgod "Drakaina"
#addgod "Man-Eater"
#addgod "Celestial Gryphon"
#addgod "Sphinx"
#addgod "Son of Fenrer"
#delgod "Statue of Order"
#delgod "Titan of Love"
#delgod "Titan of Winds and Waves"
#delgod "Titan of War and Wisdom"
#delgod "Cyclops"
#delgod "Solar Eagle"
#fortera 1
#templepic 10
#sacrificedom
#end

#selectnation 33 -- MA Arco
#addgod "Theophag"
#end

#selectnation 36 -- MA Pythium
#addgod "Theophag"
#end

#selectspell 205 --awaken tattoos
#restricted 125
#end

#selectspell 1146 --procession of the underworld
#restricted 125
#restricted 126
#end

#selectspell 1128 --bind keres
#restricted 125
#restricted 126
#end

#selectspell 420 --ancestor spirits
#restricted 125
#restricted 126
#end

#selectspell 421 -- wrath of the ancestors
#restricted 125
#restricted 126
#end

#selectspell 199 -- Monster Boar
#restricted 125
#restricted 126
#end

#selectnation 126
#clearnation
#name "Getaea"
#epithet "Brides of Night"
#era 3
#descr "Getaea was once a reformed Arcoscephalian colony claimed by the Koson descendants of Sauromatian Witch Kings. When they finally grew old, the Kosonoi were replaced by their sorceress daughters. The Scorpii now rule over the nation as its cruel and powerful queens, having revived the old ways of blood magic. Beastmen and humans live side by side, while spirits and terrible monsters lurk Getaea's dark woods. Though it was once a conflicted nation, it's people have since given in and fully embraced the pagan revelries of old. Getaeans revere wolves as a symbol of nurture and refuge, and Stygian Wolves in particular are sacred to them."
#summary "Race: Humans, satyrs and dryads.

Military: Infantry, cavalry, Asphodelian satyrs.

Magic: Blood, Nature, Water, Earth, and Death.

Priests: Average."
#brief "Getaea's Sorcerer Kings have grown old and are replaced by their blood-thirsty daughters. Its armies consist of cavalry and a blend of human and satyr infantry. Its mages are powerful dryad sorceresses who demand blood sacrifice."
#color 0.45 0.37 0.27
#secondarycolor 0.28 0.0 0.04
--#flag "getaea/flag.tga"
#startsite "Mahlic Altar"
#startsite "The Erebos Woods"
#cavenation 0
#hatesterr 216
#clearrec
#startcom 5735
#startscout 5734
#startunittype1 5725
#startunitnbrs1 15
#startunittype2 5726
#startunitnbrs2 15
#addrecunit 5725
#addrecunit 5726
#addrecunit 5727
#addrecunit 5728
#addrecunit 5729
#addrecunit 5730
#addrecunit 5731
#addrecunit 5732
#addreccom 5734
#addreccom 5735
#addreccom 5736
#addreccom 5737
#addreccom 5738
#forestcom 5739
#addreccom 5741
#hero1 5744
#hero2 5745
#hero3 5746
#multihero1 5747
#defcom1 5735
#defcom2 5738
#defunit1 5725
#defunit1b 5730
#defunit2 5731
#defmult1 10
#defmult1b 5
#defmult2 10
#defmult2b 0
#likespop 42
#likespop 78
#cleargods
#homerealm 3
#homerealm 10
#addgod "Aksei Driada"
#cheapgod20 "Aksei Driada"
#addgod "Idol of Sorcery"
#addgod "Idol of Beasts"
#addgod "Volla of the Bountiful Forest"
#addgod "Horned One"
#addgod "Drakaina"
#addgod "Man-Eater"
#addgod "Celestial Gryphon"
#addgod "Sphinx"
#addgod "Bitch Queen"
#delgod "Statue of Order"
#delgod "Titan of Love"
#delgod "Titan of Winds and Waves"
#delgod "Titan of War and Wisdom"
#delgod "Cyclops"
#delgod "Solar Eagle"
#fortera 2
#templepic 10
#sacrificedom
#end

=====New Spells=====

--#newspell 1699
--#name "Contact Solomonar"
--#descr "A suitably strong mage travels to a secluded, inscription-covered cave and negotiates the services of a Solomonar. The Solomonari are powerful air wizards, able to control clouds and the rain. While they are fiercely protective over their magical secrets, the Solomonari carry respect toward powerful air mages and may thus pledge their fealty to the Awakening God."
--#school 0
--#researchlevel 6
--#path 0 1
--#pathlevel 0 4
--#damage 5757
--#effect 10001
--#fatiguecost 2500
--#nreff 1
--#restricted 125
--#end

--#newspell 1700
--#name "VINE THINGY"
--#descr "FILLER"
--#school 2
--#researchlevel 3
--#path 0 6
--#pathlevel 0 1
--#path 1 5
--#pathlevel 1 1
--#damage 1009
--#effect 2
--#fatiguecost 15
--#flightspr 419
--#nextspell 1701
--#nreff 1002
--#range 5020
--#sound 29
--#spec 4096
--#restricted 126
--#end

--#newspell 1701
--#name "VINE THINGY SLEEPY TIME"
--#damage 5030
--#effect 3
--#spec 4096
--#end

--#newspell 1702
--#name "VINE THINGY SHITGUN"
--#descr "FILLER"
--#school 2
--#researchlevel 6
--#path 0 6
--#pathlevel 0 1
--#path 1 5
--#pathlevel 1 1
--#damage 1009
--#effect 2
--#fatiguecost 20
--#flightspr 419
--#nextspell 1701
--#nreff 2008
--#range 5025
--#sound 29
--#spec 4096
--#restricted 126
--#end

#newspell 1703
#name "Call Uriasi"
#descr "A nature mage travels to a remote forest and sacrifices a lamb to attract the attention of three Uriasi. They are giant, black minotaurs with an exceptional lifespan who have grown large on the fruits and plants of Erebos Woods. Although they are normally peaceful, they can be convinced to protect their forests and crush the enemies of the Awakening God."
#school 0
#researchlevel 6
#path 0 6
#pathlevel 0 2
#damage 5749
#effect 10001
#fatiguecost 1200
#nreff 3
#restricted 126
#end

#newspell 1704
#name "Call Stygian Wolves"
#descr "The mage burns the branch of an ash tree, opening a portal to the Stygian realm and calling several wolves to their service. Having been exposed to the river's magic, the wolves are colored black as night. Stygian Wolves are sacred and, much like Kokythiai, are difficult to harm with mundane weapons."
#school 0
#researchlevel 3
#path 0 5
#pathlevel 0 1
#path 1 2
#pathlevel 1 1
#damage 5750
#effect 10001
#fatiguecost 700
#nreff 4
#restricted 126
#end

#newspell 1705
#name "Stygian Wolf Pack"
#descr "A powerful death mage travels to the depths of Erebos Woods to find a sacred ash tree, where he sacrifices a gray wolf. The ritual opens a portal large enough to allow passage to an entire pack of Stygian Wolves, who willingly obey the caster."
#school 0
#researchlevel 6
#path 0 5
#pathlevel 0 4
#path 1 2
#pathlevel 1 2
#damage 5750
#effect 10001
#fatiguecost 2100
#nreff 1012
#restricted 126
#end

#newspell 1706
#name "Send Pricolici"
#descr "The mage casts a dark ritual, enlisting the service of a recently-dead criminal to kill her enemies. The Pricolici were sadistic and violent in life, and the reanimation ritual alters them to become appropriately bestial and savage in death. Though they were clearly once men, the Pricolici have a wolven semblance and desire nothing more than to eat the flesh of living creatures."
#school 4
#researchlevel 5
#path 0 5
#pathlevel 0 3
#damage 5751
#effect 10050
#provrange 3
#nowatertrace 1
#fatiguecost 500
#nreff 1
#restricted 126
#end

#newspell 1707
#name "Summon Strigoi"
#descr "The caster summons a Strigoi from the Underworld to serve him. The Strigoi are restless spirits, said to have once been a sons of Skögu the Man Eater. The ritual gives the spirit a physical form, and anchors him to the plane of the living. Strigoi are immortal and will return to Erebos Woods if defeated in battle. Much like he was in life, the Strigoi is a warrior and a powerful mage."
#school 0
#researchlevel 8
#path 0 5
#pathlevel 0 5
#path 1 2
#pathlevel 1 1
#damage 5752
#effect 10001
#fatiguecost 5000
#nreff 1
#restricted 126
#end

#newspell 1708
#name "Bind Sfinti"
#descr "Several Sfinti are summoned and bound by the sacrifice of human blood. The Sfinti were once the messengers of the previous Pantrokator. They were banished from his divine realm when they sought to betray him, and now walk the earth. Sfinti have insatiable appetites and once bound will devour the enemies of the Awakening God."
#school 6
#researchlevel 6
#path 0 7
#pathlevel 0 4
#damage 5753
#effect 10001
#fatiguecost 1200
#nreff 3
#restricted 126
#end
--Make these scale??

#newspell 1709
#name "Call Capcauni"
#descr "FILLER"
#school 6
#researchlevel 7
#path 0 7
#pathlevel 0 3
#path 1 6
#pathlevel 1 2
#damage 5754
#effect 10001
#fatiguecost 6000
#nreff 1
#restricted 126
#end

#newspell 1710
#name "Placate Daughter of Erebos"
#descr "The mage spends a month reveling in Getaea's umbrous forests, attracting the attention of a refugee dryad. Though she is no longer blessed with aphrodisiac beauty, both Getaea's people and its sorcerer kings are quite fond of the Driada Batran. She wields religious authority and may join the Kosonoi in religious sacrifice. The Driada gladly pledges herself to the Awakening God as a means to preserve the old Panic ways."
#fatiguecost 17
#school 5
#researchlevel 4
#path 0 6
#path 1 5
#pathlevel 0 2
#pathlevel 1 1
#effect 10021
#fatiguecost 1500
#nreff 1
#damagemon "Driada Batran"
#onlyatsite "Erebos Woods"
#restricted 125
#end


=====New Events=====

#newevent
#rarity -1
#req_maxdominion -10
#req_5monsters 5702 --getaean infanrty
#req_mydominion 0
#nation -2
#incdom 2
#unrest 10
#msg "A thunderstorm has shaken this province.  Believing this to be a sign of a False Pretender, the Getaeans present took the opportunity to curse the False Pretender quite thoroughly, and competed amongst themselves to concoct the most vicious insults.  The False Pretender now holds less sway over this land!"
#end

#newevent
#rarity -1
#req_maxdominion -10
#req_5monsters 5702 --getaean infantry
#req_mydominion 0
#nation -2
#unrest 10
#msg "A thunderstorm has shaken this province.  Believing this to be a sign of a False Pretender, the Getaeans present took the opportunity to curse the False Pretender quite thoroughly, and competed amongst themselves to concoct the most vicious insults.  The winner of the contest ceased his cursing only when a thunderbolt struck him down, and the other contestants rapidly followed suit."
#killmon 5702
#end

#newevent
#rarity -1
#req_maxdominion -10
#req_5monsters 5702 --getaean infantry
#req_mydominion 0
#nation -2
#unrest 10
#msg "A thunderstorm has shaken this province.  Believing this to be a sign of a False Pretender, the Getaeans present took the opportunity to curse the False Pretender quite thoroughly, and competed amongst themselves to concoct the most vicious insults.  The False Pretender responded with a earth-shaking thunderbolt as a prize for the victor, yet the blasphemer survived!  He is proclaimed a holy champion, and given status as such."
#killmon 5702
#com 5758
#newdom 3
#end

--#newevent
--#rarity 0
--#req_rare 85
--#req_unique 1
--#req_fornation 125
--#req_capital 0
--#req_temple 1
--#req_forest 1
--#req_freesites 1
--#req_monster 5716 -- Koson
--#req_nositenbr 1617 -- Erebos Woods
--#nation -2
--#msg "Refugee dryads have arrived in the forest, calling it Erebos Woods. The dark rituals of your Koson have attracted their attention and they are quite taken with his sorcerous talent. The Koson has negotiated their fealty and they are now willing to serve the Awakening God."
--#addsite 1617
--#end

#newevent
#rarity -2
#req_rare 30
#req_unique 1
#req_fornation 125
#req_temple 1
#req_targmnr 5717
#nation -2
#msg "The populace has become even more enamored with the Driada Matriarch. She has been formally anointed as high priestess and her religious authority has increased."
#pathboost 8
#end

#newevent
#rarity -1
#req_unique 1
#req_fornation 125
#req_capital 0
#req_forest 1
#req_freesites 1
#req_monster 5717
#req_nositenbr 1617 -- Erebos Woods
#nation -2
#msg "The Driada Matriarch has established a sanctum for her Asphodelian brothers and sisters. The refugees have erected a temple in your honor."
#addsite 1618
#end

--#newevent
--#rarity -2
--#req_unique 1
--#req_fornation 125
--#req_capital 0
--#req_land 1
--#req_freesites 1
--#req_nearbysite 1
--#req_nositenbr 1619
--#nation -2
--#msg "Several satyrs from the nearby Chancel Forest have set up camp and are training for war. [Satyr Encampment]"
--#addsite 1619
--#end

#newevent
#rarity 0
#req_rare 20
#req_unique 1
#req_fornation 125
#req_monster "Theophag"
#req_monster "Returned King"
#req_gem 7
#nation -2
#msg "Skögu has challenged his son to a gory eating contest. While the former Man Eater easily won, he and his son ate several blood slaves before they were full."
#gemloss 7
#end

#newevent
#rarity 0
#req_fornation 125
#req_monster "Lupoaica"
#req cold 1
#msg "A Lupoaica has taken it upon herself to provide comfort to some of the local farmers.  Sharing her warmth and her food with many of the locals has caused unrest to fall and growth to rise."
#incscale3 1
#unrest -10
#end

#newevent
#rarity 0
#req_fornation 125
#req_monster "
#end

--List of proposed changes for MA Getaea
--Light inf -> peltasti, foreign-rec cav
--Convert sacred heavy cav to sacred light cav
--Astral xpath on dishealers
--Solomonari to mtn rec
--Foreign-rec cavalry/peltasti??
--Aima a shit, plz buff
--A LOT of nature magic
--Up the odds at D5 on Kosonoi to make it a tart nation, maybe to 50%
--Make Kosonoi non STR
--100% of 2 points in nature, death, or water on Kosonoi
--Coastal-rec hoplites
--Buffed ancestral spirits
--Erebos Woods procs an event that ups black dryads somehow, possibly make the dryad return to the cap
--Check chassis list
--Satyr peltasti forest-rec???
--pierce only???
--Mount the Aima Theiko and up his combat stats
--Jade Kyrias gain a 20% chance of NWB1
--XPshape on regular cataphracts to make them Jade catas
--Make the aima theiko some kind of ancestor intermediary like the enaries??
--add luck events to the vanators/lupaioca

--List of proposed changes for LA Getaea
--NOTHING

--Confirmed MA changes for 1.3
--Forest-rec satyr peltasti, coastal-rec hoplitai  DONE
--More soro cavalry in forts, nonsacred cataphracts and slightly more light cavalry variety  DONE
--Getaean Infantry gained sica instead of a spear  DONE
--Getaean light infantry changed to Getaean Peltasts  DONE
--Jade Kyrias gain a 20% chance of NWB1  DONE
--Boosted Jade Kyria's and Cataphract's stats slightly, +1 HP, morale and MR for the Cata, +2 HP, +1 att/def and +2 mr for the Kyria  DONE
--Dumped Jade cataphract's defense by a point  DONE
--Standard catas will digivolve into jade cataphracts once they hit 150 xp.  I hope.

--Confirmed MA changes for 1.4
--Fucked up and turned cataphracts into tc archers somehow
--1.4 was a mistake
--Sorry Sotha

--Confirmed MA changes for 1.5
--Fixed some bugs

--Confirmed LA changes for 1.3
--NOTHING

--Confirmed MA changes for 1.6
--Dropped Jade Kyria cost by a bunch, since elves often cost less
--Regular catas #xpshape into jade ones at 30 xp
--Gave Driada Matriarch #expertleader

--Confirmed LA changes for 1.7
--Sacreds with limb-chopping falxes or sicas <- this did not happen, I was on crack.

--Confirmed MA changes for 1.7
--Reworked Vraci to be f3 and also made Aimata Theikou s1d1.

--Confirmed MA changes for 1.8
--Made solomonari mtnrec
--Jam and toast best breakfast
--Bugfixes
--xpshape probably fixed
--Solomonari given to autocalc
--Aimata fixed
--made summons work again
